// Tile represents a tile in the screen
class Tile {
 constructor(position, filled, dom) {
  this.position = position;
  this.filled = filled;
  this.dom = dom;
 }

 appendToScreen() {
  document.querySelector('#screen').appendChild(this.dom);
 }
}

// Position represents the X and Y coordinates for each tile on the screen
class Position {
 constructor(x, y) {
  this.x = x;
  this.y = y;
 }
}

const populateScreen = screen => {
 for (let i = 0; i < 10; i++) {
  for (let j = 0; j < 20; j++) {
   const position = new Position(i, j);
   const DOM = document.createElement('div');
   DOM.id = `${i}${j}`;
   DOM.className = 'tile';
   const tile = new Tile(position, false, DOM);
 
   screen.push(tile);
   tile.appendToScreen();
  }
 }
}

export default {
 populateScreen
}