import { populateScreen } from './screen.js'

// Screen is the place where the tiles are painted
screen = [];
populateScreen(screen);

const tileColors = {
 teal: 'teal',
 blue: 'blue',
 orange: 'orange',
 yellow: 'yellow',
 green: 'green',
 purple: 'purple',
 red: 'red'
};

const fps = 1000 /60;

const getRandomColor = _ => {
 const keys = Object.keys(tileColors);
 return tileColors[keys[keys.length * Math.random() << 0]];
};

setInterval(_ => {
 screen.forEach(tile => tile.dom.className = 'tile ' + getRandomColor());
}, fps)